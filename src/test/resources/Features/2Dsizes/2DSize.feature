Feature: As a user I want to do create 2D sizes

#  @RegressionCICD

Scenario: User create 2d sizes
    Given User launches centric application
    When Setup Enum for size dimensions in setup page
      |Enumeration|
      |DimensionType (1)|
    Then Create the Enum value for the sizes
      |Enumeration Value| |Description|
      |Length           | |2DSize     |
      |Waist            | |2DSize     |
    Then Go to homepage
    And verify user screen
    When User creates multiple sizes with different type for TwoDSizes
      |Size| |DimensionType | |SortOrder |
      |30  | |Length        | |01        |
      |32  | |Length        | |02        |
      |34  | |Length        | |03        |
      |30A | |Waist         | |04        |
      |32B | |Waist         | |05        |
      |32C | |Waist         | |06        |

