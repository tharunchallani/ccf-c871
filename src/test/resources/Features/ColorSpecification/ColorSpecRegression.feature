Feature: As a user I want to do regression cases for the color specification module

#  @RegressionCICD

  Scenario:
    Given User launches centric application
    When User navigates to Home page of centric
    Then Click on specification tab and create color specification
          |Color Specification Name| Code | Description|
          |Reg_color1              |022   |RegressionValidation|
          |Reg_color2              |023   |RegressionValidation|
    And Merge the color specification and validate the status after merging
    Then Validate the options like copy and delete
          |Color Specification Name|
          |Reg_colorCopy           |
    When User delete the merged colour spec and verify the error message
    Then User creates the Custom view and add the options to the table
          |Available Attributes|
          |RGB                 |
          |RGB Hex             |
    But User deletes the unwanted attributes from custom views column
          |Selected Attributes|
          |Pantone            |
          |Pantone TC         |
          |Libraries          |
          |Tags               |
    And User enters the RBG value and verify the colour in the table
          |Color Specification Name| |RGB Hex|
          |Reg_color2              | |#0000FF|
          |Reg_color1              | |#FF0000|
