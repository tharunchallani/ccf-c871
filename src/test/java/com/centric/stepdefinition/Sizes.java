package com.centric.stepdefinition;

import com.centric.objectrepository.HomePage;
import com.centric.objectrepository.PopupPage;
import com.centric.objectrepository.SetupPage;
import com.centric.objectrepository.SpecificationPage;
import com.centric.resources.Commonactions;

import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Th;
import io.cucumber.datatable.DataTable;
import org.apache.logging.log4j.core.config.Order;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

public class Sizes extends Commonactions {

    Commonactions ca = new Commonactions();
    SetupPage sp = new SetupPage();
    PopupPage pp = new PopupPage();
    HomePage hp = new HomePage();
    SpecificationPage spec = new SpecificationPage();

    @When("Setup Enum for size dimensions in setup page")
    public void setupEnumForSizeDimensionsInSetupPage(DataTable dt) throws InterruptedException {
        jsWaitForPageLoad();
        ca.click(sp.getUser_Enumerations());
        Thread.sleep(3000);
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        int length = list.size();
        for (int i = 0; i < length; i++) {
            ca.click(sp.getUser_EnumDropBox());
            Thread.sleep(2000);
            WebElement element = driver.findElement(By.xpath("//label[contains(text(),'" + list.get(i).get("Enumeration") + "')]"));
            Thread.sleep(200);
            element.click();
            ca.click(sp.getUser_Enumerations());
        }
    }

    @Then("Create the Enum value for the sizes")
    public void createTheEnumValueForTheSizes(DataTable dt) throws Throwable {
        ca.jsScrollPageDown(sp.getUser_NewEnumValue());
        if (sp.getUser_NewEnumValue().isDisplayed()) {
            List<Map<String, String>> list = dt.asMaps(String.class, String.class);
            int length = list.size();
            for (int i = 0; i < length; i++) {
                jsWaitForPageLoad();
                ca.click(sp.getUser_NewEnumValue());
                Thread.sleep(200);
                ca.insertText(sp.getUser_NewEnumName(), list.get(i).get("Enumeration Value"));
                Thread.sleep(200);
                ca.insertText(sp.getUser_NewEnumDescription(), list.get(i).get("Description"));
                Thread.sleep(200);
                ca.click(sp.getPopUpSaveBtn());
                Thread.sleep(200);
            }
        }
    }

    @When("User creates multiple sizes with different type for TwoDSizes")
    public void userCreatesMultipleSizesWithDifferentTypeForTwoDSizes(DataTable dataTable) throws InterruptedException {
        ca.click(hp.getSpecificationTab());
        jsWaitForPageLoad();
        ca.click(spec.getSizeTab());
        jsWaitForPageLoad();
        if (spec.getNewSizeAction().isDisplayed()) {
            List<Map<String, String>> list = dataTable.asMaps(String.class, String.class);
            int length = list.size();
            for (int i = 0; i <= length; i++) {
                Random random = new Random();
                Thread.sleep(200);
                ca.click(spec.getNewSizeAction());
                Thread.sleep(200);
                ca.insertText(spec.getSizeInputBox(), list.get(i).get("Size"));
                WebElement dimension = spec.getDimensionBox();
                dimension.clear();
                ca.insertText(spec.getDimensionBox(), list.get(i).get("DimensionType"));
                Thread.sleep(200);
                Actions a = new Actions(driver);
                a.sendKeys(Keys.DOWN).build().perform();
                a.sendKeys(Keys.ENTER).build().perform();
                ca.insertText(spec.getSortOrderBox(),list.get(i).get("Sort Order"));
                Thread.sleep(200);
                ca.click(sp.getPopUpSaveBtn());
            }
        }
    }
}

