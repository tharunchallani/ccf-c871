package com.centric.stepdefinition;

import com.centric.dataProviders.ConfigFileReader;
import com.centric.dataProviders.HomePageReader;
import com.centric.dataProviders.LoginFileReader;
import com.centric.dataProviders.SpecificationTabReader;
import com.centric.objectrepository.HomePage;
import com.centric.objectrepository.PopupPage;
import com.centric.objectrepository.SetupPage;
import com.centric.objectrepository.SpecificationPage;
import com.centric.resources.Commonactions;
import cucumber.api.java.Before;
import cucumber.api.java.en.*;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


import java.awt.*;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class ColorSpecification extends Commonactions {

    Commonactions ca = new Commonactions();
    SetupPage sp = new SetupPage();
    PopupPage pp = new PopupPage();
    HomePage hp = new HomePage();
    SpecificationPage spec = new SpecificationPage();


    @Given("Login to Centric application with valid credentials")
    public void loginToCentricApplicationWithValidCredentials() throws InterruptedException {
        ConfigFileReader configFileReader = new ConfigFileReader();
        LoginFileReader loginFileReader = new LoginFileReader();
        //System.setProperty("driver", configFileReader.getDriverPath());
        driver.get(configFileReader.getApplicationUrl1());
        //driver.manage().window().maximize();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(loginFileReader.getUsername())));
        Boolean version = driver.findElement(By.xpath(configFileReader.getversion())).isDisplayed();
        System.out.println("The version of the application - " + " " + version.booleanValue());
        driver.findElement(By.cssSelector(loginFileReader.getUsername())).sendKeys("Administrator");
        driver.findElement(By.cssSelector(loginFileReader.getpassword())).sendKeys("centric8");
        WebElement login = driver.findElement(By.xpath(loginFileReader.getlogin()));
        login.submit();
    }

    @When("User navigates to Home page of centric")
    public void userNavigatesToHomePageOfCentric() throws InterruptedException {
        ConfigFileReader configFileReader = new ConfigFileReader();
        LoginFileReader loginFileReader = new LoginFileReader();
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(configFileReader.getversion())));
        Thread.sleep(3000);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(loginFileReader.gethomebtn()))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(configFileReader.getversion())));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        String expectedHeader = "Centric 8";
        String message = driver.findElement(By.xpath(loginFileReader.getheader())).getText();
        Assert.assertTrue("Centric 8", message.contains(expectedHeader));
        System.out.println("C8 user is successfully verified ");
    }

    @Then("Click on specification tab and create color specification")
    public void clickOnSpecificationTab(DataTable dt) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        HomePageReader homePageReader = new HomePageReader();
        ConfigFileReader configFileReader = new ConfigFileReader();
        SpecificationTabReader specificationTabReader = new SpecificationTabReader();
        Thread.sleep(3000);
        driver.findElement(By.xpath(homePageReader.getSpecificationTab())).click();
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath(homePageReader.getSpecificationTab()))).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath(specificationTabReader.getColorSpecification())).click();
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getColorSpecification()))).click();
        Thread.sleep(3000);
        WebElement element = driver.findElement(By.xpath(specificationTabReader.getNewColorSpecification()));
        element.click();
        if (element.isDisplayed() && element.isEnabled()) {
            String expectedHeader = "New Color Specification";
            String message = driver.findElement(By.xpath(specificationTabReader.getHeaderCS())).getText();
            Assert.assertTrue("New Color Specification", message.contains(expectedHeader));
            System.out.println("Color Specification header verified Successfully");
            driver.findElement(By.cssSelector(specificationTabReader.getCancelCS())).click();
        }
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        int length = list.size();
        //System.out.println(length);
        for (int i = 0; i < length; i++) {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getNewColorSpecification()))).click();
            Thread.sleep(1000);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getColorSpecificationName()))).sendKeys(list.get(i).get("Color Specification Name"));
            driver.findElement(By.xpath(specificationTabReader.getSaveBtn())).click();
            Thread.sleep(1000);
            WebElement nameNode = driver.findElement(By.xpath("//a[@class='browse' and text()='" + list.get(i).get("Color Specification Name") + "']"));
            WebElement values = driver.findElement(By.xpath("(//a[@class='browse' and text()='" + list.get(i).get("Color Specification Name") + "'])/parent::td"));
            String storeNode = values.getAttribute("data-csi-url");
            //System.out.println(storeNode);
            WebElement codeCell = driver.findElement(By.xpath("//tr[@data-csi-result='" + storeNode + "']/td[@data-csi-heading='Code::0']"));
            codeCell.click();
            Thread.sleep(1000);
            WebElement inputCode = driver.findElement(By.xpath("//div/textarea"));
            inputCode.sendKeys(list.get(i).get("Code"));
            Thread.sleep(1000);
            driver.findElement(By.xpath(specificationTabReader.getColorSpecification())).click();
            Thread.sleep(1000);
            WebElement description = driver.findElement(By.xpath("//tr[@data-csi-result='" + storeNode + "']/td[@data-csi-heading='Description::0']"));
            description.click();
            Thread.sleep(1000);
            WebElement descriptionInput = driver.findElement(By.xpath("//div/textarea"));
            descriptionInput.sendKeys(list.get(i).get("Description"));
            Thread.sleep(1000);
            driver.findElement(By.xpath(specificationTabReader.getColorSpecification())).click();
        }
    }

    @And("Merge the color specification and validate the status after merging")
    public void doValidationLikeMergeCopyDeleteForTheCreatedColorSpecification() throws InterruptedException, AWTException {
        String name = "Reg_color1";
        WebDriverWait wait = new WebDriverWait(driver, 15);
        SpecificationTabReader specificationTabReader = new SpecificationTabReader();
        java.util.List<WebElement> optionCount = driver.findElements(By.xpath("//span[@data-csi-act='ReplaceColorSpecifications']"));
        optionCount.size();
        driver.findElement(By.xpath("(//span[@data-csi-act='ReplaceColorSpecifications'])[1]")).click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.findElement(By.xpath("//td[text()='Reg_color1']")).click();
        WebElement save = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getSaveBtn())));
        save.click();
        Thread.sleep(3000);
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        WebElement activeStatus = driver.findElement(By.xpath("//a[@class='browse' and text()='" + name + "']//following::td[1]"));
        jse.executeScript("arguments[0].scrollIntoView(true);", activeStatus);
        WebElement statusChecker = driver.findElement(By.xpath("//a[@class='browse' and text()='" + name + "']//following::input[@aria-checked='false']"));
        Boolean check = statusChecker.isSelected();
        if (check.equals(false)) {
            System.out.println("The color specification merge was successful");
        } else {
            System.out.println("The color specification merge was not successful");
        }
    }

    @Then("Validate the options like copy and delete")
    public void validateThOptionsLikeCopyDelete(DataTable dt) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        SpecificationTabReader specificationTabReader = new SpecificationTabReader();
        java.util.List<WebElement> optionCount = driver.findElements(By.xpath("//span[@data-csi-act='Copy']"));
        optionCount.size();
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); i++) {
            Thread.sleep(200);
            driver.findElement(By.xpath("(//span[@data-csi-act='Copy'])[1]")).click();
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            WebElement copyOption = wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getColorSpecificationName())));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            copyOption.clear();
            Thread.sleep(200);
            copyOption.sendKeys(list.get(i).get("Color Specification Name"));
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            Boolean save_Copy = driver.findElement(By.cssSelector(specificationTabReader.getSave_CopyCS())).isDisplayed();
            System.out.println("The Popup contains Save&Copy button" + " -" + save_Copy);
            Boolean cancel = driver.findElement(By.cssSelector(specificationTabReader.getCancelCS())).isDisplayed();
            System.out.println("The Popup contains Cancel button" + "- " + cancel);
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.findElement(By.xpath(specificationTabReader.getSaveBtn())).click();
        }
        String CS_copy = "Reg_colorCopy";
        WebElement delete = driver.findElement(By.xpath("//a[@class='browse' and text()='" + CS_copy + "']//following::span[@data-csi-act='Delete']"));
        Thread.sleep(1000);
        delete.click();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//span[contains(text(),'Delete')])[2]")).click();
        System.out.println("The copy color specification is deleted ");
    }

    @When("User delete the merged colour spec and verify the error message")
    public void userDeleteTheMergedColourSpecAndVerifyTheErrorMessage() throws InterruptedException {
        String name = "Reg_color2";
        WebElement delete = driver.findElement(By.xpath("//a[@class='browse' and text()='" + name + "']//following::span[@data-csi-act='Delete']"));
        Thread.sleep(2000);
        delete.click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("(//span[contains(text(),'Delete')])[2]")).click();
        Thread.sleep(1000);
        String errorMessage = driver.findElement(By.xpath("//div[@class='csi-message-txt']")).getText();
        System.out.println("User unable to delete the merged color spec, please find the error message -" + " " + errorMessage);
        Thread.sleep(1000);
        driver.findElement(By.xpath("//span[contains(text(),'OK')]")).click();
    }

    @Then("User creates the Custom view and add the options to the table")
    public void userCreatesTheCustomViewAndAddTheOptionsToTheTable(DataTable dt) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        SpecificationTabReader specificationTabReader = new SpecificationTabReader();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(specificationTabReader.getCustomViewIcon()))).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath(specificationTabReader.getManageViewsOption())).click();
        Thread.sleep(2000);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(specificationTabReader.getCopyCV()))).click();
        Thread.sleep(2000);
        WebElement CV_Name = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(specificationTabReader.getCopyNameBox())));
        CV_Name.clear();
        Thread.sleep(1000);
        CV_Name.sendKeys("CV_Copy");
        //List<WebElement> attributeValues = driver.findElement(By.xpath(specificationTabReader.getAttributeListBox()));
        //attributeValues.size();
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        for (int i = 0; i < list.size(); i++) {
            Select dropdown = new Select(driver.findElement(By.xpath(specificationTabReader.getAttributeListBox())));
            Thread.sleep(1000);
            dropdown.selectByVisibleText(list.get(i).get("Available Attributes"));
            Thread.sleep(1000);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getCV_AddBtn()))).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath(specificationTabReader.getCV_SaveBtn())).click();
        Thread.sleep(3000);
    }

    @And("User enters the RBG value and verify the colour in the table")
    public void userEntersTheRBGValueAndVerifyTheColourInTheTable(DataTable dt) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        int length = list.size();
        for (int i = 0; i < length; i++) {
            WebElement nameNode = driver.findElement(By.xpath("//a[@class='browse' and text()='" + list.get(i).get("Color Specification Name") + "']"));
            WebElement values = driver.findElement(By.xpath("(//a[@class='browse' and text()='" + list.get(i).get("Color Specification Name") + "'])/parent::td"));
            String storeNode = values.getAttribute("data-csi-url");
            WebElement codeCell = driver.findElement(By.xpath("//tr[@data-csi-result='" + storeNode + "']/td[@data-csi-heading='RGBHex::0']"));
            //System.out.println(codeCell);
            codeCell.click();
            Thread.sleep(1000);
            WebElement inputCode = driver.findElement(By.xpath("//div/textarea"));
            inputCode.sendKeys(list.get(i).get("RGB Hex"));
            Thread.sleep(1000);
            SpecificationTabReader specificationTabReader = new SpecificationTabReader();
            driver.findElement(By.xpath(specificationTabReader.getColorSpecification())).click();
            Thread.sleep(2000);
        }
        System.out.println("The RBG colour is has been successfully added to the colour specification");
    }

    @But("User deletes the unwanted attributes from custom views column")
    public void userDeletesTheUnwantedAttributesFromCustomViewsColumn(DataTable dt) throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, 15);
        SpecificationTabReader specificationTabReader = new SpecificationTabReader();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.getCustomViewIcon()))).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath(specificationTabReader.getManageViewsOption())).click();
        Thread.sleep(2000);
        List<Map<String, String>> list = dt.asMaps(String.class, String.class);
        for (int i = 0; i <list.size() ; i++) {
            Select dropdown = new Select(driver.findElement(By.xpath(specificationTabReader.get_SelectedAttributeBox())));
            Thread.sleep(2000);
            dropdown.selectByVisibleText(list.get(i).get("Selected Attributes"));
            Thread.sleep(2000);
        }
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(specificationTabReader.get_RemoveBtn()))).click();
        Thread.sleep(1000);
        driver.findElement(By.xpath(specificationTabReader.getCV_SaveBtn())).click();
        System.out.println("The custom view is created successfully with the given options");

    }
}

