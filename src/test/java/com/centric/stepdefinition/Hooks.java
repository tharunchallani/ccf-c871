package com.centric.stepdefinition;

import java.io.File;
import java.util.Collections;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.centric.resources.Commonactions;
import com.relevantcodes.extentreports.ExtentReports;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class Hooks extends Commonactions {
	
	Commonactions ca = new Commonactions();
	private static final String ITestResult = null;
	public static ExtentReports extent;
	com.relevantcodes.extentreports.ExtentTest logger;
	
	@Before
	public void before() {
		 ca.launch("http://2k12r2-qa-2:8080/WebAccess/home.html");

	}
	
	@After
	public void after() {

		
	}

}
