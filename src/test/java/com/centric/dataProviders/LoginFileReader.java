package com.centric.dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class LoginFileReader {

    private final String loginPage= "src/test/java/com/centric/ConfigPages/LoginPage.properties";
    private Properties properties;

    //Login Page
    public LoginFileReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(loginPage));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + loginPage);
        }
    }

    public String getUsername() {
        String username_txtbx = properties.getProperty("username_txtbx");
        if(username_txtbx != null) return username_txtbx;
        else throw new RuntimeException("username xpath has been not found or changed.");
    }

    public String getpassword() {
        String password_txtbx = properties.getProperty("password_txtbx");
        if(password_txtbx != null) return password_txtbx;
        else throw new RuntimeException("password xpath has been not found or changed.");
    }

    public String getlogin() {
        String login_btn = properties.getProperty("login_btn");
        if(login_btn != null) return login_btn;
        else throw new RuntimeException("login xpath has been not found or changed.");
    }
    public String gethomebtn() {
        String homeIcon = properties.getProperty("homeIcon");
        if(homeIcon != null) return homeIcon;
        else throw new RuntimeException("HomeIcon is not clicked.");
    }
    public String getheader() {
        String headertxt = properties.getProperty("headertxt");
        if(headertxt != null) return headertxt;
        else throw new RuntimeException("HomeIcon is not clicked.");
    }
}
