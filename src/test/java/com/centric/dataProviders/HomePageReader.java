package com.centric.dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class HomePageReader {

    private Properties properties;
    private final String propertyFilePath= "src/test/java/com/centric/ConfigPages/homePage.properties";

    public HomePageReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public String getSpecificationTab(){
        String SpecificationTab = properties.getProperty("SpecificationTab");
        if(SpecificationTab!= null) return SpecificationTab;
        else throw new RuntimeException("driverPath not specified in the Configuration.properties file.");
    }
}
