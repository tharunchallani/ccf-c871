package com.centric.dataProviders;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class SpecificationTabReader {

    private Properties properties;
    private final String propertyFilePath= "src/test/java/com/centric/ConfigPages/SpecificationPage.properties";

    public SpecificationTabReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
        }
    }

    public String getColorSpecification(){
        String ColorSpecification = properties.getProperty("ColorSpecification");
        if(ColorSpecification!= null) return ColorSpecification;
        else throw new RuntimeException("ColorSpecification not specified in the Configuration.properties file.");
    }

    public String getNewColorSpecification(){
        String NewColorSpec_btn = properties.getProperty("NewColorSpec_btn");
        if(NewColorSpec_btn!= null) return NewColorSpec_btn;
        else throw new RuntimeException("NewColorSpec_btn not specified in the Configuration.properties file.");
    }

    public String getColorSpecificationName(){
        String ColorSpecInputBox = properties.getProperty("ColorSpecInputBox");
        if(ColorSpecInputBox!= null) return ColorSpecInputBox;
        else throw new RuntimeException("ColorSpecInputBox not specified in the Configuration.properties file.");
    }

    public String getSaveBtn(){
        String SaveBtn = properties.getProperty("SaveBtn");
        if(SaveBtn!= null) return SaveBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getHeaderCS(){
        String VerifyHeaderCS = properties.getProperty("VerifyHeaderCS");
        if(VerifyHeaderCS!= null) return VerifyHeaderCS;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getCancelCS(){
        String CancelBtn = properties.getProperty("CancelBtn");
        if(CancelBtn!= null) return CancelBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getSave_CopyCS(){
        String Save_CopyBtn = properties.getProperty("Save_CopyBtn");
        if(Save_CopyBtn!= null) return Save_CopyBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getCustomViewIcon(){
        String CustomViewBtn = properties.getProperty("CustomViewBtn");
        if(CustomViewBtn!= null) return CustomViewBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getManageViewsOption(){
        String ManageViews_CV = properties.getProperty("ManageViews_CV");
        if(ManageViews_CV!= null) return ManageViews_CV;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getCopyCV(){
        String CopyCV_Option = properties.getProperty("CopyCV_Option");
        if(CopyCV_Option!= null) return CopyCV_Option;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getCopyNameBox(){
        String CopyNameTxt_bx = properties.getProperty("CopyNameTxt_bx");
        if(CopyNameTxt_bx!= null) return CopyNameTxt_bx;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String getAttributeListBox(){
        String AttributesBox = properties.getProperty("AttributesBox");
        if(AttributesBox!= null) return AttributesBox;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String getCV_AddBtn(){
        String CV_AddBtn = properties.getProperty("CV_AddBtn");
        if(CV_AddBtn!= null) return CV_AddBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String getCV_SaveBtn(){
        String CV_PopUpSaveBtn = properties.getProperty("CV_PopUpSaveBtn");
        if(CV_PopUpSaveBtn!= null) return CV_PopUpSaveBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String get_SizeTab(){
        String SizeTab = properties.getProperty("SizeTab");
        if(SizeTab!= null) return SizeTab;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String get_NewSize(){
        String NewSize_ActionBtn = properties.getProperty("NewSize_ActionBtn");
        if(NewSize_ActionBtn!= null) return NewSize_ActionBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String get_SizeNameTxtBox(){
        String SizeNameInput_txt = properties.getProperty("SizeNameInput_txt");
        if(SizeNameInput_txt!= null) return SizeNameInput_txt;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String get_DimensionTypeArrowBtn(){
        String DimensionType = properties.getProperty("DimensionType");
        if(DimensionType!= null) return DimensionType;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String get_DimensionBox(){
        String DimensionBox = properties.getProperty("DimensionBox");
        if(DimensionBox!= null) return DimensionBox;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
    public String get_SelectedAttributeBox(){
        String SelectedAttributesBox = properties.getProperty("SelectedAttributesBox");
        if(SelectedAttributesBox!= null) return SelectedAttributesBox;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String get_RemoveBtn(){
        String CV_RemoveBtn = properties.getProperty("CV_RemoveBtn");
        if(CV_RemoveBtn!= null) return CV_RemoveBtn;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }

    public String get_AttributeTab(){
        String AttributeTabCV = properties.getProperty("AttributeTabCV");
        if(AttributeTabCV!= null) return AttributeTabCV;
        else throw new RuntimeException("SaveBtn not specified in the Configuration.properties file.");
    }
}
