package com.centric.objectrepository;

import com.centric.resources.Commonactions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class SpecificationPage extends Commonactions{

	public SpecificationPage() {
		PageFactory.initElements(Commonactions.driver, this);
	}
	

	@FindBy(how = How.CSS,using = "span[data-csi-tab='ApparelViews-Sizes']")
	private WebElement SizeTab;


	@FindBy(how = How.CSS,using = "table[data-csi-automation='plugin-ApparelViews-Sizes-ToolbarNewActions']")
	private WebElement NewSizeAction;

	@FindBy(how = How.CSS,using = "[data-csi-automation='field-ProductSize-Form-Node Name'] .dijitInputInner")
	private WebElement SizeInputBox;

	@FindBy(how = How.CSS,using = "[data-csi-automation='field-ProductSize-Form-DimensionType'] .dijitArrowButton")
	private WebElement DimensionType;

	@FindBy(how = How.XPATH,using = "//div[@data-csi-automation='field-ProductSize-Form-DimensionType'][1]//input[@class='dijitReset dijitInputInner']")
	private WebElement DimensionBox;

	@FindBy(how = How.CSS,using = "[data-csi-automation='field-ProductSize-Form-SizeCode'] .dijitInputInner")
	private WebElement SortOrderBox;

	public WebElement getSortOrderBox() {
		return SortOrderBox;
	}

	public WebElement getDimensionBox() {
		return DimensionBox;
	}

	public WebElement getDimensionType() {
		return DimensionType;
	}

	public WebElement getSizeTab() {
		return SizeTab;
	}

	public WebElement getNewSizeAction() {
		return NewSizeAction;
	}

	public WebElement getSizeInputBox() {
		return SizeInputBox;
	}
}
