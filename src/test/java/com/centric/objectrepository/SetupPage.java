package com.centric.objectrepository;

import com.centric.resources.Commonactions;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import javax.swing.text.html.CSS;

public class SetupPage extends Commonactions {

    public SetupPage() {
        PageFactory.initElements(Commonactions.driver, this);
    }

    @FindBy(how = How.CSS, using = "span[data-csi-tab='Site-Enumerations']")
    private WebElement user_Enumerations;

    @FindBy(how = How.XPATH, using = "//span[@data-csi-automation='filter-Site-Enumerations-Node Name']")
    private WebElement User_EnumDropBox;

    @FindBy(how = How.CSS, using = "table[data-csi-automation='plugin-EnumList-Values-ToolbarNewActions']")
    private WebElement User_NewEnumValue;

    @FindBy(how = How.CSS,using = "div[data-csi-automation='field-EnumValue-Form-Node Name'] .dijitInputInner")
    private WebElement User_NewEnumName;

    @FindBy(how = How.CSS,using = "div[data-csi-automation='field-EnumValue-Form-Description'] .dijitInputInner")
    private WebElement User_NewEnumDescription;

    @FindBy(how = How.CSS,using = "span[data-csi-act='Save']")
    private WebElement PopUpSaveBtn;

    public WebElement getPopUpSaveBtn() {
        return PopUpSaveBtn;
    }

    public WebElement getUser_Enumerations() {
        return user_Enumerations;
    }

    public WebElement getUser_EnumDropBox() {
        return User_EnumDropBox;
    }

    public WebElement getUser_NewEnumValue() {
        return User_NewEnumValue;
    }

    public WebElement getUser_NewEnumName() {
        return User_NewEnumName;
    }

    public WebElement getUser_NewEnumDescription() {
        return User_NewEnumDescription;
    }
}
